import { $fetch, FetchOptions, FetchError } from "ofetch";

const CSRF_COOKIE = "XSRF-TOKEN";
const CSRF_HEADER = "X-XSRF-TOKEN";
export const ACCESS_TOKEN="ACCESS_TOKEN"

// could not import these types from ofetch, so copied them here
interface ResponseMap {
  blob: Blob;
  text: string;
  arrayBuffer: ArrayBuffer;
}
type ResponseType = keyof ResponseMap | "json";
// end of copied types

export type APIFetch<R extends ResponseType> = FetchOptions<R> & {
  redirectIfNotAuthenticated?: boolean;
  redirectIfNotVerified?: boolean;
};

export async function $apiFetch<T, R extends ResponseType = "json">(
  path: RequestInfo,
  {
    redirectIfNotAuthenticated = true,
    redirectIfNotVerified = true,
    ...options
  }: APIFetch<R> = {}
) {
  const { backendUrl, frontendUrl } = useRuntimeConfig().public;
  const router = useRouter();
  let token = useCookie(ACCESS_TOKEN).value;

  let headers: any = {
    accept: "application/json",
    ...options?.headers,
    ...(token && { 'Authorization': `Bearer ${token}` }),
  };

  console.log(headers)

  if (process.server) {
    headers = {
      ...headers,
      ...useRequestHeaders(["cookie"]),
      referer: frontendUrl,
    };
  }

  try {
    return await $fetch<T, R>(path, {
      baseURL: backendUrl,
      ...options,
      headers,
      credentials: "include",
    });
  } catch (error) {
    if (!(error instanceof FetchError)) throw error;

    // when any of the following redirects occur and the final throw is not caught then nuxt SSR will log the following error:
    // [unhandledRejection] Error [ERR_HTTP_HEADERS_SENT]: Cannot set headers after they are sent to the client

    const status = error.response?.status ?? -1;

    if (redirectIfNotAuthenticated && [401, 419].includes(status)) {
      await router.push("/login");
    }

    if (redirectIfNotVerified && [409].includes(status)) {
      await router.push("/verify-email");
    }

    if ([500].includes(status)) {
      console.error(`[Server API ${backendUrl} Error]`, error.data?.message, error.data);
    }

    throw error;
  }
}

function getCookie(name: string) {
  const match = document.cookie.match(
    new RegExp("(^|;\\s*)(" + name + ")=([^;]*)")
  );
  return match ? decodeURIComponent(match[3]) : null;
}
