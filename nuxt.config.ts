export default defineNuxtConfig({
  modules: [
    '@pinia/nuxt',
  ],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  runtimeConfig: {
    public: {
      backendUrl: process.env.NUXT_PUBLIC_BACKEND_URL,
      frontendUrl: "http://localhost:6060",
      camdigkeyUrl: process.env.CAMDIGIKEY_URL,
      invoiceFileUrl: process.env.INVOICE_FILE_URL,
      apiUrl: "http://localhost:3004"
    },
  },
  css: [
    '@/assets/css/main.css',
  ],
  imports: {
    dirs: ["./utils"],
  },
})