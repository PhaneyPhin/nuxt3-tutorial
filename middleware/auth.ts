export default defineNuxtRouteMiddleware(async () => {
  const user = useUser();
  const route = useRoute()

  if (!user.value) {
    const { camdigkeyUrl } = useRuntimeConfig().public;

    const { data } = await fetch(`${camdigkeyUrl}/login-token`).then(res => res.json())
    navigateTo(data.loginUrl, { external: true })
  }
});
