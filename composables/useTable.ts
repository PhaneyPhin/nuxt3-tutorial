import { PaginationParam } from "~/types/Model";
import { Pagination, Response } from "~/types/Response";

export const useTable = <T>(url: string, params: PaginationParam ={ take: 10 }) => {
  const list = ref<Pagination<T>>();
  const loading = ref(false);
  const pagesList = ref<number[]>([])

  onMounted(async () => {
    loading.value = true;

    fetch(1)
  });

  const fetch = async (page: number) => {
    const data = await $apiFetch<Pagination<T>>(url, {
        method: "get",
        params: {
            page: page,
            take: params.take
        }
      });
  
      loading.value = false;
      list.value = data;
      console.log(data)
      console.log(data?.lastPage)
      pagesList.value = Array(data.lastPage || 1).fill(null).map((item, index) => index +1)
  }

  const remove = async (id: number) => {
    return await $apiFetch(`${url}/${id}`, {
      method: 'delete'
    })
  }

  return {
    remove,
    list,
    loading,
    pagesList,
    fetch
  };
};
