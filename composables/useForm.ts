interface UseFormReturnObject<T> {
    form: T,
    submit: () => Promise<any> 
}

export const useForm = <T extends Object>(url: string, data: T) : UseFormReturnObject<T> =>  {
    const form: any = reactive(data)

    const submit = async () => {
      
        const formData = new FormData();
        
        for ( var key in form) {
            formData.append(key, form[key]);
        }

        const response: any = await $apiFetch(url, {
            method: 'POST',
            body: formData,
            redirect: 'follow'
        })

        return response
    }

    return {
        form,
        submit
    }
}