import { ACCESS_TOKEN } from "~/utils/$apiFetch";

export type User = {
  name: string;
  email?: string;
};

export type LoginCredentials = {
  username: string;
  password: string;
  remember: boolean
};

export type RegisterCredentials = {
  name: string;
  email: string;
  password: string;
  password_confirmation: string;
};

export type ResetPasswordCredentials = {
  email: string;
  password: string;
  password_confirmation: string;
  token: string;
};

type AccessTokenResponse = {
  error: number;
  message: string;
  data: {
    accessToken: string;
  };
};

interface LoginResponse {
  userId: number;
  accessToken: string;
}
// Value is initialized in: ~/plugins/auth.ts
export const useUser = () => {
  return useState<User | undefined | null>("user", () => undefined);
};

export const useCamDigiKeyLogin = () => {
  return useCookie<Boolean | undefined | null>("camdigikey");
};

export const useAuth = () => {
  const router = useRouter();
  const token = useCookie(ACCESS_TOKEN);
  const isCamdigiKeyLogin = useCamDigiKeyLogin();
  const user = useUser();
  const isLoggedIn = computed(() => !!user.value);
  const { camdigkeyUrl } = useRuntimeConfig().public;

  async function refresh() {
    try {
      user.value = await fetchCurrentUser();
    } catch {
      user.value = null;
    }
  }

  async function login(credentials: LoginCredentials) {
    isCamdigiKeyLogin.value = false;
    console.log(isCamdigiKeyLogin.value)
    if (isLoggedIn.value) return;

    let data = await $apiFetch<LoginResponse>("/auth/login", {
      method: "post",
      body: credentials,
    });
   
    token.value = data.accessToken;
    nextTick(async () => {
      await refresh()
      navigateTo('/dashboard')
    });
  }

  async function register(credentials: RegisterCredentials) {
    await $apiFetch("/register", { method: "post", body: credentials });
    await refresh();
  }

  async function resendEmailVerification() {
    return await $apiFetch<{ status: string }>(
      "/email/verification-notification",
      {
        method: "post",
      }
    );
  }

  async function logout() {
    if (!isLoggedIn.value) return;

    // await $apiFetch("/logout", { method: "post" });
    token.value = null;
    user.value = null;

    nextTick(() => {
      refresh()
    });
  }

  async function forgotPassword(email: string) {
    return await $apiFetch<{ status: string }>("/forgot-password", {
      method: "post",
      body: { email },
    });
  }

  async function resetPassword(credentials: ResetPasswordCredentials) {
    return await $apiFetch<{ status: string }>("/reset-password", {
      method: "post",
      body: credentials,
    });
  }

  async function loginViaCamdigiKey(authToken: any) {
    const response = await $apiFetch<AccessTokenResponse>("auth/access-token", {
      method: "post",
      body: {
        authToken
      }
    });

    token.value = response.data.accessToken;
    isCamdigiKeyLogin.value = true;

    nextTick(async () => {
      await refresh()
      navigateTo('/dashboard')
    });
  }

  return {
    user,
    isLoggedIn,
    isCamdigiKeyLogin,
    login,
    register,
    resendEmailVerification,
    logout,
    forgotPassword,
    loginViaCamdigiKey,
    resetPassword,
    refresh,
  };
};

export const fetchCurrentUser = async () => {
  try {
    const isCamdigiKeyLogin= useCamDigiKeyLogin();
    const { camdigkeyUrl } = useRuntimeConfig().public
    const token = useCookie(ACCESS_TOKEN)

    const result = await $apiFetch<User>("/auth/me", {
      redirectIfNotAuthenticated: false,
      headers: {
        'Authorization': `Bearer ${token.value}`
      },
      query: {
        isCamdigiKeyLogin: isCamdigiKeyLogin.value
      },
    });

    return result
  } catch (error: any) {
    if ([401, 419, 403].includes(error?.response?.status)) return null;
    throw error;
  }
};

const useUsers = () => {
  return useState("users");
};
