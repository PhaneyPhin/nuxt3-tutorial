export type User = {
    name: string,
    email: string
}

export type InvoiceItem = {
    id: string,
    productName: string,
    price: number,
    quantity: number
}

export type Invoice = {
    id: string,
    sender: User,
    invoiceNumber: string,
    receiver: User,
    currency: string,
    downloadUrl: string,
    invoiceItems: InvoiceItem[]
    total?: number,
    createdAt: string,
    paidAt: Date,
}

export type Product = {
    id: number,
    name: string,
    price: number,
    image: string
}

export type CartItem = {
    product: Product,
    quantity: number
}

export interface PaginationParam {
    page?: number,
    take?: number
}