export interface Response<T> {
    data: Pagination<T>,
}

export interface Pagination<T> {
    data: T[],
    currentPage: number,
    lastPage: number,
    total: number,
    from: number, 
    to: number
}